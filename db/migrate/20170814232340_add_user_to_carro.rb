class AddUserToCarro < ActiveRecord::Migration[5.1]
  def change
    add_reference :carros, :user, foreign_key: true
  end
end
