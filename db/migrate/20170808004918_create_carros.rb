class CreateCarros < ActiveRecord::Migration[5.1]
  def change
    create_table :carros do |t|
      t.string :marca
      t.string :modelo
      t.integer :anio
      t.string :clase
      t.string :duenio

      t.timestamps
    end
  end
end
