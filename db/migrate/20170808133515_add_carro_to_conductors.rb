class AddCarroToConductors < ActiveRecord::Migration[5.1]
  def change
    add_reference :conductors, :carro, foreign_key: true
  end
end
