class CreateConductors < ActiveRecord::Migration[5.1]
  def change
    create_table :conductors do |t|
      t.string :nombre
      t.string :apellido
      t.string :correo

      t.timestamps
    end
  end
end
