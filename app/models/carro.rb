class Carro < ApplicationRecord
    has_many :conductors
    belongs_to :user, optional: true
end
