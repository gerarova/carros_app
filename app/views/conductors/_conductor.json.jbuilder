json.extract! conductor, :id, :nombre, :apellido, :correo, :created_at, :updated_at
json.url conductor_url(conductor, format: :json)
