Rails.application.routes.draw do
  
    devise_for :users, controllers: {
      sessions: 'users/sessions'
      }
  resources :carros do resources :conductors
  end
  get "/", to: "carros#index"
  get "users/list", to: "carros#user", as: "list_user"
    root to: 'carros#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

